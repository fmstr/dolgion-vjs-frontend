import jsx from "./jsx";
import Router, { RouterOutlet } from "./modules/router";

import "./scss/index";
import Header from "./components/Header";
import Loading from "./pages/Loading";
import Redirect from "./pages/Redirect";

const App = () => (
  <>
    <Header />
    <RouterOutlet />
  </>
);
jsx.render(<App />, document.getElementById("root"));

Router.register(
  [
    {
      path: "/r/:to/:id",
      component: Redirect
    },
    {
      cache: true,
      path: "/",
      asyncComponent: () =>
        import(/* webpackChunkName: "home" */ "./pages/Home/index.js")
    },
    {
      cache: true,
      path: "/register",
      title: "Бүртгүүлэх",
      asyncComponent: () =>
        import(/* webpackChunkName: "register" */ "./pages/auth/Register")
    },
    {
      cache: true,
      path: "/forgot",
      title: "Нууц үг сэргээх",
      asyncComponent: () =>
        import(/* webpackChunkName: "forgot" */ "./pages/auth/Forgot")
    },
    {
      cache: true,
      path: "/login",
      title: "Нэвтрэх",
      asyncComponent: () =>
        import(/* webpackChunkName: "login" */ "./pages/auth/Login")
    },
    {
      path: "/profile/videos/:id",
      component: require("./pages/Profile/Videos").default
      // asyncComponent: () =>
      //   import(/* webpackChunkName: "profile:videos" */ "./pages/Profile/Videos")
    },
    {
      path: "/live/:id",
      asyncComponent: () =>
        import(/* webpackChunkName: "live" */ "./pages/Live")
      // component: require("./pages/Live").default
    },
    {
      path: "/no-hls",
      asyncComponent: () =>
        import(/* webpackChunkName: "no-hls" */ "./pages/errors/no-hls")
    }
  ],
  {
    fallback: {
      asyncComponent: () =>
        import(/* webpackChunkName: "404" */ "./pages/errors/404")
    },
    loading: Loading
  }
);
