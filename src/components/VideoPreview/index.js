import jsx from "@/jsx";
import LazyImg from "@/modules/lazy-offscreen-img";
import { RouterLink } from "@/modules/router";
import { parseTime } from "@/util";

import styles from "./styles";
import animations from "@/scss/animations";
import { bgCover } from "@/scss";

export default ({ id, images, title, userBasicDto, createdAt }) => {
  if (!id || !images || !title)
    return (
      <div class={styles.videoPreview}>
        <div class={[styles.thumbnailWrapper, animations.loading]} />
        <div class={[styles.videoTitleLoading, animations.loading]} />
      </div>
    );
  const imagesLength = images.length;
  if (imagesLength === 0 || !images[imagesLength - 1].thumbnailImagePath)
    return <></>;
  return (
    <div class={styles.videoPreview}>
      <RouterLink href={`/live/${id}`} class={styles.link}>
        <div class={[bgCover, styles.thumbnailWrapper, animations.loading]}>
          <LazyImg
            src={`https://api.dolgion.com/dolgion${
              images[imagesLength - 1].thumbnailImagePath
            }`}
            onError={node => {
              node.hidden = true;
              node.parentNode.classList.add(styles.fallback);
              node.parentNode.classList.remove(animations.loading);
            }}
            onLoad={node =>
              node.parentNode.classList.remove(animations.loading)
            }
          />
          <div style={styles.fallback} />
        </div>
        <div class={styles.videoTitle}>{title}</div>
      </RouterLink>
      {userBasicDto && createdAt && (
        <div class={styles.videoData}>
          <span>{parseTime(createdAt)}</span>
          <RouterLink href={`/profile/videos/${userBasicDto.id}`}>
            <span class={styles.username}>@{userBasicDto.username}</span>
          </RouterLink>
        </div>
      )}
    </div>
  );
};
