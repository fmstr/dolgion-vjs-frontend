import jsx from "@/jsx";
import styles from "./styles";

export default ({ class: className, ...props }) => (
  <div class={jsx.mergeClasses(styles["lds-ripple"], className)} {...props}>
    <div />
    <div />
  </div>
);
