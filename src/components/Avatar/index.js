import jsx from "@/jsx";
import styles from "./styles";

export default ({ src, class: className, ...props }) => (
  <img
    class={jsx.mergeClasses(styles.avatar, className)}
    src={
      src
        ? "https://api.dolgion.com/dolgion" + src
        : "/assets/defaultAvatar.png"
    }
    {...props}
  />
);
