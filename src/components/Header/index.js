import jsx from "@/jsx";
import { appState } from "@/state";
import { deleteChildren } from "@/util";
import { RouterLink, Redirect } from "@/modules/router";

import Input from "@/components/Input";
import Avatar from "../Avatar";
import styles from "./styles";

const KEY_ENTER = 13;
const SearchBar = () => (
  <div class={styles.searchBarWrapper}>
    <Input
      placeholder="Хайх"
      onKeyup={(node, event) => {
        if (event.keyCode !== KEY_ENTER || !node.value) return;
        Redirect(`/search/video/${node.value}`);
      }}
    />
  </div>
);

const UserProfileButton = ({ avatar }) => {
  return (
    <div class={styles.userProfile}>
      <Avatar src={avatar} />
    </div>
  );
};

const rightPanel = <div />;
appState.onSet("user", user => {
  let avatar = null;
  if (user.picture.length > 0)
    avatar = user.picture[user.picture.length - 1].thumbnailImagePath;
  requestAnimationFrame(() => {
    deleteChildren(rightPanel);
    rightPanel.append(<UserProfileButton {...{ avatar }} />);
  });
});
if (!appState.token)
  rightPanel.append(
    <div class={styles.group}>
      <RouterLink href="/register">
        <div class={styles.registerLink}>Бүртгүүлэх</div>
      </RouterLink>
      <RouterLink href="/login">
        <div class={styles.loginButton}>
          <img src="/assets/icons/user.svg" />
        </div>
      </RouterLink>
    </div>
  );

export default () => (
  <div class={styles.header}>
    <div class={styles.group}>
      <RouterLink href="/">
        <div class={styles.logoWrapper}>
          <img src="/assets/logo.png" />
        </div>
      </RouterLink>
      <SearchBar />
    </div>
    <div class={styles.group}>{rightPanel}</div>
  </div>
);
