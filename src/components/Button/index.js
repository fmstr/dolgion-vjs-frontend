import jsx from "@/jsx";
import styles from "./styles";

export default ({ class: className, children, ...props }) => (
  <button class={jsx.mergeClasses(styles.button, className)} {...props}>
    {children}
  </button>
);
