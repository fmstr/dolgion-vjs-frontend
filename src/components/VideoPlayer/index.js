import jsx from "@/jsx";
import useState from "@/modules/reactive";
import { Redirect } from "@/modules/router";
import { debounceAnimationFrame } from "@/util";

import styles from "./styles";
import LoadingProgress from "../LoadingProgress";

const Button = ({ src, ...props }) => (
  <div class={styles.button} {...props}>
    <img src={src} />
  </div>
);

function normailzeTime(time) {
  let minutes = Math.floor(time / 60);
  if (minutes < 0) minutes = 0;
  let seconds = Math.floor(time % 60);
  if (seconds < 0) seconds = 0;
  return (
    (minutes < 10 ? "0" + minutes : minutes) +
    ":" +
    (seconds < 10 ? "0" + seconds : seconds)
  );
}

export default ({ id, src, autoplay, class: className }) => {
  const [
    durationInfoRef,
    volumeButtonRef,
    pauseButtonRef,
    mutedButtonRef,
    playButtonRef,
    bufferingRef,
    controlsRef,
    progressRef,
    videoRef
  ] = jsx.createRefs(9);

  let state = useState({
    playing: undefined,
    muted: undefined
  });
  state.on("playing", (playing, oldValue) => {
    if (playing === oldValue) return;
    requestAnimationFrame(() => {
      if (!controlsRef.current.hidden) {
        playButtonRef.current.hidden = playing;
        pauseButtonRef.current.hidden = !playing;
      }
    });
  });
  state.on("muted", (muted, oldValue) => {
    if (muted === oldValue) return;
    videoRef.current.muted = muted;
    requestAnimationFrame(() => {
      mutedButtonRef.current.hidden = !muted;
      volumeButtonRef.current.hidden = muted;
    });
  });

  const playVideo = () => {
    if (!videoRef.current.paused) return;
    videoRef.current.play();
  };
  const pauseVideo = () => {
    if (videoRef.current.paused) return;
    videoRef.current.pause();
  };

  const onMouseMovement = debounceAnimationFrame((_, event) => {
    const time = normailzeTime(
      videoRef.current.duration *
        (event.offsetX / (event.target.offsetWidth - 15))
    );
    const offset = event.offsetX - 25 + "px";
    durationInfoRef.current.innerText = time;
    durationInfoRef.current.style.left = offset;
    durationInfoRef.current.hidden = false;
  });
  const changeDuration = event => {
    const elementWidth = event.target.clientWidth;
    videoRef.current.currentTime =
      videoRef.current.duration * (event.offsetX / elementWidth);
  };
  const updateTime = () => {
    const progress =
      (videoRef.current.currentTime / videoRef.current.duration) * 100;
    requestAnimationFrame(
      () => (progressRef.current.style.width = progress + "%")
    );
  };

  const dragStart = (node, event) => {
    pauseVideo();
    state.playing = false;
    changeDuration(event);
    node.onmousemove = node.ontouchmove = event => {
      updateTime();
      changeDuration(event);
    };
  };
  const dragEnd = node => {
    playVideo();
    node.onmousemove = node.ontouchmove = null;
    state.playing = true;
  };

  const ProgressBar = ({ ref }) => (
    <div class={styles.progressBarWrapper}>
      <div
        class={styles.progressBar}
        onMouseOver={onMouseMovement}
        onMouseMove={onMouseMovement}
        onMouseLeave={() =>
          requestAnimationFrame(() => (durationInfoRef.current.hidden = true))
        }
        onMouseDown={dragStart}
        onMouseUp={dragEnd}
        onClick={(_, event) => changeDuration(event)}
      >
        <div class={styles.durationInfo} ref={durationInfoRef} hidden />
        <div class={styles.progress} ref={ref} />
      </div>
    </div>
  );

  return (
    <div
      class={jsx.mergeClasses(styles.videoWrapper, className)}
      onMount={() => {
        document.body.onkeydown = e => {
          if (!videoRef.current || !document.body.contains(videoRef.current)) {
            document.body.onkeydown = null;
            return;
          }
          switch (e.key) {
            case "ArrowLeft":
              videoRef.current.currentTime -= 5;
              return;
            case "ArrowRight":
              videoRef.current.currentTime += 5;
              return;
            case " ":
              if (videoRef.current.paused) playVideo();
              else pauseVideo();
              return false;
            case "0":
              videoRef.current.currentTime = 0;
              return;
          }
        };
      }}
    >
      <div class={styles.player}>
        <div class={styles.buffering} ref={bufferingRef}>
          <LoadingProgress />
        </div>
        <video
          onLoadedMetadata={video => {
            requestAnimationFrame(() => {
              videoRef.current.controls = false;
              controlsRef.current.hidden = false;
            });
            const promise = video.play();
            if (promise)
              promise.catch(() =>
                requestAnimationFrame(() => {
                  controlsRef.current.remove();
                  videoRef.current.controls = true;
                  bufferingRef.current.hidden = true;
                })
              );
            else if (!autoplay) video.pause();
          }}
          onPlay={() => (state.playing = true)}
          onPause={() => (state.playing = false)}
          onWaiting={() => (bufferingRef.current.hidden = false)}
          onPlaying={() => (bufferingRef.current.hidden = true)}
          onClick={() => {
            if (state.playing) pauseVideo();
            else playVideo();
          }}
          onTimeUpdate={updateTime}
          onMount={node => {
            if (src) {
              node.src = src;
              return;
            }
            import(/* webpackChunkName: "hls.js" */ "hls.js").then(
              ({ default: hls }) => {
                if (!hls.isSupported()) Redirect("/no-hls");
                const Hls = new hls();
                Hls.loadSource(
                  `https://stream.dolgion.com/live/${id}/playlist.m3u8?DVR`
                );
                Hls.attachMedia(node);
              }
            );
          }}
          ref={videoRef}
        />
        <div ref={controlsRef} class={styles.controls} hidden>
          <Button
            src="/assets/player/play.svg"
            onClick={playVideo}
            ref={playButtonRef}
          />
          <Button
            src="/assets/player/pause.svg"
            onClick={pauseVideo}
            ref={pauseButtonRef}
            hidden
          />
          <ProgressBar ref={progressRef} />
          <div class={styles.controlsRight}>
            <Button
              src="/assets/player/volume.svg"
              onClick={() => (state.muted = true)}
              ref={volumeButtonRef}
            />
            <Button
              src="/assets/player/mute.svg"
              onClick={() => (state.muted = false)}
              ref={mutedButtonRef}
              hidden
            />
            <Button
              src="/assets/player/fullscreen.svg"
              onClick={() => {
                for (let fnName of [
                  "requestFullscreen",
                  "webkitRequestFullscreen",
                  "mozRequestFullScreen",
                  "oRequestFullscreen",
                  "msRequestFullscreen"
                ]) {
                  if (videoRef.current[fnName]) {
                    videoRef.current[fnName]();
                    break;
                  }
                }
              }}
            />
          </div>
        </div>
      </div>
    </div>
  );
};
