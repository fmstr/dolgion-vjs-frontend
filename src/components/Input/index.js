import jsx from "@/jsx";
import styles from "./styles";

export default ({ class: className, ...props }) => (
  <input class={jsx.mergeClasses(className, styles.input)} {...props} />
);
