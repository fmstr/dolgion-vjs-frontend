import useState from "@/modules/reactive";

export const appState = useState({
  token: window.localStorage.getItem("token"),
  user: null
});

appState.on("token", token => window.localStorage.setItem("token", token));
appState.onSet("token", token => {
  fetch("https://api.dolgion.com/dolgion/secure/user/info", {
    method: "POST",
    headers: {
      Authorization: "Bearer " + token
    }
  })
    .then(res => res.json())
    .then(res => (appState.user = res))
    .catch(() => (appState.token = null));
});
