const eventList = {
  onPlay: "play",
  onLoad: "load",
  onPause: "pause",
  onClick: "click",
  onError: "error",
  onKeyup: "keyup",
  onInput: "input",
  onSubmit: "submit",
  onMouseUp: "mouseup",
  onWaiting: "waiting",
  onPlaying: "playing",
  onTouchEnd: "touchend",
  onMouseDown: "mousedown",
  onMouseOver: "mouseover",
  onMouseMove: "mousemove",
  onMouseLeave: "mouseleave",
  onTouchStart: "touchstart",
  onTimeUpdate: "timeupdate",
  onLoadedMetadata: "loadedmetadata"
};

const eventListener = (fn, node) => event => fn(node, event);

function applyProps(node, props) {
  if (props.dangerouslySetInnerHTML) {
    node.innerHTML = props.dangerouslySetInnerHTML;
    delete props.dangerouslySetInnerHTML;
  }
  let onMount;
  if (props.onMount) {
    onMount = props.onMount;
    delete props.onMount;
  }

  if (props.class) {
    if (props.class instanceof Array)
      node.classList = props.class.filter(x => !!x).join(" ");
    else node.classList = props.class;
    delete props.class;
  }

  delete props.children;
  for (let propName in props) {
    if (eventList[propName])
      node.addEventListener(
        eventList[propName],
        eventListener(props[propName], node)
      );
    else if (propName.startsWith("data-") || propName.startsWith("aria-"))
      node.setAttribute(propName, props[propName]);
    else node[propName] = props[propName];
  }
  if (onMount) onMount(node);
}

export function mergeClasses(c1, c2) {
  let result = [];
  if (c1 instanceof Array) result.push(...c1);
  else if (typeof c1 === "string") result.push(c1);
  if (c2 instanceof Array) result.push(...c2);
  else if (typeof c2 === "string") result.push(c2);
  return result;
}

export const hFrag = { __internal: true };
export const Fragment = hFrag;

const parseChild = (el, child) => {
  if (child === null || child === undefined) return;
  if (child instanceof Array) child.forEach(parseChild.bind(this, el));
  else el.append(child);
};

export function h(component, props, ...children) {
  if (!component) return;
  let el;
  if (component.__internal) el = document.createDocumentFragment();
  if (typeof component === "function")
    return component(children ? { ...props, children } : props);
  if (typeof component === "string") el = document.createElement(component);
  if (!el) return;
  parseChild(el, children);
  if (props) {
    if (props.ref) {
      props.ref.current = el;
      delete props.ref;
    }
    applyProps(el, props);
  }
  return el;
}

export function createRef() {
  return { __ref: true, current: null };
}
export function createRefs(len) {
  return Array(len)
    .fill()
    .map(createRef);
}

export default {
  h,
  hFrag,
  Fragment,
  createRef,
  createRefs,
  mergeClasses,
  render: (node, root) => root.append(node)
};
