import jsx from "@/jsx";

const observer = new IntersectionObserver(entries => {
  entries.forEach(entry => {
    if (!entry.isIntersecting && entry.intersectionRatio < 0.8) return;
    observer.unobserve(entry.target);

    const applyNode = node => {
      if (!node) return;
      node.$render = entry.target.$render;
      node.$root = entry.target.$root;
      requestAnimationFrame(() => {
        node.$root.append(node);
        observer.observe(node);
      });
    };
    const newNode = entry.target.$render();
    if (newNode instanceof Promise) newNode.then(applyNode);
    else applyNode(newNode);
  });
});

export default ({ render, ...props }) => {
  const el = <div {...props} />;
  el.$render = render;
  el.$root = el;
  observer.observe(el);
  return el;
};
