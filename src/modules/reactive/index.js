export default (initialState, constants) => {
  let state = { ...constants };
  let subscribes = new Map();

  state.on = (key, callback) => {
    if (subscribes[key]) subscribes[key].push(callback);
    else subscribes[key] = [callback];
  };
  state.onSet = (key, callback) => {
    const newCallback = (v, old) => {
      if (v !== undefined && v !== null) callback(v, old);
    };
    if (subscribes[key]) subscribes[key].push(newCallback);
    else subscribes[key] = [newCallback];
    if (state[key] !== undefined && state[key] !== null)
      newCallback(state[key]);
  };

  Object.keys(initialState).forEach(key => {
    let value = initialState[key];
    Object.defineProperty(state, key, {
      get: () => value,
      set: newVal => {
        const oldVal = value;
        value = newVal;
        if (subscribes[key]) subscribes[key].forEach(fn => fn(newVal, oldVal));
      }
    });
  });
  return state;
};
