import jsx from "@/jsx";
import { routerOutletRef, loadRoute, register } from "./route";

export function Redirect(to) {
  if (location.href === to || location.pathname === to) return;
  history.pushState(null, null, to);
  loadRoute();
}

export const RouterOutlet = () => (
  <div ref={routerOutletRef} data-router-outlet="" />
);
export const RouterLink = ({ children, href, ...props }) => (
  <a
    href={href}
    onClick={(node, e) => {
      e.preventDefault();
      e.stopPropagation();
      Redirect(node.href);
    }}
    {...props}
  >
    {children}
  </a>
);

export default {
  register,
  routerOutletRef,
  Redirect,
  RouterLink,
  RouterOutlet
};
