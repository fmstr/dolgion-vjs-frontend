import jsx from "@/jsx";
import RouteRegex from "route-parser";
import { deleteChildren } from "@/util";

export const routerOutletRef = jsx.createRef();
export const defaultTitle = document.title;
export let loadRoute = () => null;

window.addEventListener("popstate", e => {
  e.preventDefault();
  e.stopPropagation();
  loadRoute();
});

let fallbackNode;
export function register(routes, defaultComponents) {
  const { loading, fallback } = defaultComponents || {};
  const paths = routes.map(route => ({
    ...route,
    node: null,
    path: new RouteRegex(route.path)
  }));

  const renderNodeInOutlet = node => {
    deleteChildren(routerOutletRef.current);
    if (!node) return;
    routerOutletRef.current.append(node);
  };

  loadRoute = () => {
    if (!routerOutletRef.current) return;
    let loadingTimeout;
    if (loading)
      loadingTimeout = setTimeout(
        () => requestAnimationFrame(() => renderNodeInOutlet(loading())),
        1000
      );

    for (let route of paths) {
      let routeMatch = route.path.match(location.pathname);
      if (!routeMatch || (!route.component && !route.asyncComponent)) continue;

      const renderRoute = node => {
        clearTimeout(loadingTimeout);
        if (route.cache) route.node = node;
        if (route.title) document.title = route.title;
        else if (document.title !== defaultTitle) document.title = defaultTitle;
        requestAnimationFrame(renderNodeInOutlet.bind(this, node));
      };

      if (route.cache && route.node) return renderRoute(route.node);

      if (route.asyncComponent) {
        route
          .asyncComponent()
          .then(({ default: component }) =>
            renderRoute(component({ route: routeMatch }))
          )
          .catch(error => {
            if (route.error) renderRoute(route.error({ error }));
          });
        return;
      }

      renderRoute(route.component({ route: routeMatch }));
      return true;
    }

    if (document.title !== defaultTitle) document.title = defaultTitle;
    if (fallback) {
      const renderFallback = () => {
        clearTimeout(loadingTimeout);
        requestAnimationFrame(renderNodeInOutlet.bind(this, fallbackNode));
      };
      if (!fallbackNode) {
        if (fallback.asyncComponent) {
          fallback.asyncComponent().then(({ default: FallbackComponent }) => {
            fallbackNode = <FallbackComponent />;
            renderFallback();
          });
          return;
        }
        fallbackNode = fallback.component();
        return renderFallback();
      }
      renderFallback();
    }
  };
  loadRoute();
}
