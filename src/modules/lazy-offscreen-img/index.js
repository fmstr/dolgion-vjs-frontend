import jsx from "@/jsx";

const observer = new IntersectionObserver(entries => {
  entries.forEach(entry => {
    if (!entry.isIntersecting && entry.intersectionRatio < 0.3) return;
    entry.target.src = entry.target.dataset.src || entry.target.src;
    delete entry.target.dataset.src;
    observer.unobserve(entry.target);
    entry.target.hidden = true;
  });
});

export default ({ src, onLoad, ...props }) => {
  const img = (
    <img
      {...props}
      data-src={src}
      onLoad={(node, event) => {
        node.hidden = false;
        onLoad(node, event);
      }}
    />
  );
  observer.observe(img);
  return img;
};
