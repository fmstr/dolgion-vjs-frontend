import jsx from "@/jsx";
import VScroll from "@/modules/vscroll";
import useState from "@/modules/reactive";

import styles from "./styles";
import ProfilePage from "./ProfilePage";
import VideoPreview from "@/components/VideoPreview";

export default ({ route: { id } }) => {
  let state = useState(
    {
      cover: undefined,
      avatar: undefined,
      loaded: undefined,
      username: undefined,
      followers: undefined,
      loadingVideos: undefined
    },
    { id, moreVideos: true, page: 1, videos: [], currentVideo: 0 }
  );

  fetch("https://api.dolgion.com/dolgion/public/user/profile", {
    method: "POST",
    body: JSON.stringify({ id }),
    headers: { "Content-Type": "application/json" }
  })
    .then(res => res.json())
    .then(({ data }) => {
      state.cover =
        (data.coverPicture &&
          data.coverPicture.length > 0 &&
          `https://api.dolgion.com/dolgion${
            data.coverPicture[0].originalFilePath
          }`) ||
        null;
      state.avatar =
        (data.picture &&
          data.picture.length > 0 &&
          `https://api.dolgion.com/dolgion${
            data.picture[0].originalFilePath
          }`) ||
        null;
      document.title = `${data.username} | Dolgion.com`;
      state.followers = data.listFollowers;
      state.username = data.username;
      state.loaded = true;
    });

  const loadVideos = cb => {
    state.loadingVideos = true;
    fetch("https://api.dolgion.com/dolgion/public/video/list", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        filterDto: { data: { isSave: 1, userId: id } },
        itemsPerPage: 12,
        pageNumber: state.page
      })
    })
      .then(res => res.json())
      .then(({ data: { pagedItems, totalRow } }) => {
        state.page++;
        state.loadingVideos = false;
        if (totalRow < 12) state.moreVideos = false;
        state.videos.push(...(pagedItems || []));
        if (cb) cb();
      });
  };

  return (
    <ProfilePage state={state}>
      <VScroll
        class={styles.videoList}
        onMount={node => {
          state.onSet("loadingVideos", loadingVideos => {
            if (loadingVideos)
              for (let i = 0; i < 3; i++) node.append(<VideoPreview />);
            else for (let i = 0; i < 3; i++) node.removeChild(node.lastChild);
          });
        }}
        render={() => {
          if (!state.moreVideos && state.currentVideo >= state.videos.length)
            return;
          if (state.currentVideo < state.videos.length) {
            const rNode = (
              <VideoPreview {...state.videos[state.currentVideo]} />
            );
            state.currentVideo++;
            return rNode;
          }
          return new Promise(resolve => {
            loadVideos(() => {
              if (state.videos.length === state.currentVideo) {
                state.moreVideos = false;
                return resolve();
              }
              const r = <VideoPreview {...state.videos[state.currentVideo]} />;
              state.currentVideo++;
              resolve(r);
            });
          });
        }}
      />
    </ProfilePage>
  );
};
