import jsx from "@/jsx";
import { loadImg } from "../../util";

import styles from "./styles";
import animations from "@/scss/animations";

import UserWidget from "./UserWidget";

export default ({ state, children }) => {
  return (
    <div class={styles.profile}>
      <div
        class={[styles.profileCoverPicture, animations.loading]}
        onMount={node =>
          state.onSet("cover", src => {
            if (src === null) {
              node.classList.remove(animations.loading);
              node.style.backgroundColor = "#ddd";
            } else
              loadImg(src).then(() => {
                node.append(<img src={src} />);
                node.classList.remove(animations.loading);
              });
          })
        }
      />
      <UserWidget state={state} />
      {children}
    </div>
  );
};
