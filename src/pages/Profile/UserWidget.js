import jsx from "@/jsx";
import { appState } from "@/state";
import { RouterLink } from "@/modules/router";
import { loadImg, setBgImage } from "@/util";

import Button from "@/components/Button";
import styles from "./styles";
import animations from "@/scss/animations";
import { bgCover } from "@/scss";

export default ({ state }) => {
  const { id } = state;
  const followButtonRef = jsx.createRef();
  return (
    <div class={styles.userWidget}>
      <div class={styles.flexRow}>
        <div
          class={[
            styles.userProfilePictureWrapper,
            bgCover,
            animations.loading
          ]}
          onMount={node =>
            state.onSet("avatar", src => {
              if (state.avatar === null) {
                node.classList.remove(animations.loading);
                setBgImage(node, "/assets/defaultAvatar.png");
              } else
                loadImg(src).then(() => {
                  setBgImage(node, src);
                  node.classList.remove(animations.loading);
                });
            })
          }
        />
        <div
          class={[
            styles.userDetails,
            ...(state.loaded ? [] : [styles.userDetailsLoading])
          ]}
          onMount={node => {
            state.onSet("loaded", () =>
              node.classList.remove(styles.userDetailsLoading)
            );
          }}
        >
          <div
            class={[styles.username, animations.loading]}
            onMount={node =>
              state.onSet("username", username => {
                node.innerText = username;
                node.classList.remove(animations.loading);
              })
            }
          >
            {state.username || ""}
          </div>
          <div
            class={animations.loading}
            onMount={node => {
              state.onSet("followers", followers => {
                node.innerText =
                  (followers && `${followers.length} дагагч`) || "";
                node.classList.remove(animations.loading);
              });
            }}
          />
        </div>
      </div>
      <div class={styles.profileLinks}>
        <RouterLink href={`/profile/videos/${id}`}>Видео</RouterLink>
        <RouterLink href={`/profile/events/${id}`}>Эвэнтүүд</RouterLink>
        <RouterLink href={`/profile/followed/${id}`}>Дагаж буй</RouterLink>
        <RouterLink href={`/profile/followers/${id}`}>Дагагчид</RouterLink>
      </div>
      <div class={styles.buttonWrapper}>
        <Button class={styles.button} ref={followButtonRef}>
          <img class={styles.followersIcon} src="/assets/icons/followers.svg" />
          <div class={styles.followLabel}>
            <span>Дагах</span>
            <span
              class={styles.subCount}
              onMount={node =>
                state.onSet(
                  "followers",
                  followers =>
                    (node.innerText = (followers && followers.length) || 0)
                )
              }
            />
          </div>
        </Button>
      </div>
    </div>
  );
};
