import jsx from "@/jsx";
import { deleteChildren } from "@/util";
import { RouterLink } from "@/modules/router";

import styles from "./styles";
import VideoPreview from "@/components/VideoPreview";

export default ({ description, name, id }) => {
  const contentRef = jsx.createRef();

  fetch("https://api.dolgion.com/dolgion/public/video/list", {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      filterDto: { data: { isSave: 1, categoryId: id } },
      itemsPerPage: 8,
      pageNumber: 1
    })
  })
    .then(res => res.json())
    .then(({ data: { pagedItems: videos } }) => {
      deleteChildren(contentRef.current);
      requestAnimationFrame(() =>
        videos.forEach(video => {
          contentRef.current.append(<VideoPreview {...video} />);
        })
      );
    });

  return (
    <div>
      <div class={styles.categoryInfo}>
        <h1>
          <RouterLink href={`/category/${id}`}>{name}</RouterLink>
        </h1>
        <span class={styles.description}>{description}</span>
      </div>
      <div class={styles.separator}>
        <span />
      </div>
      <div class={[styles.categoryContent, styles.videoList]} ref={contentRef}>
        {Array(4)
          .fill()
          .map(() => (
            <VideoPreview />
          ))}
      </div>
    </div>
  );
};
