import jsx from "@/jsx";
import { RouterLink } from "@/modules/router";
import { deleteChildren } from "@/util";

import styles from "./styles";
import Avatar from "@/components/Avatar";
import { loading } from "@/scss/animations";

export default () => {
  const [listRef, categoryListRef] = jsx.createRefs(2);
  fetch("https://api.dolgion.com/dolgion/public/leaderboard/makers/lastmonth")
    .then(res => res.json())
    .then(({ data }) =>
      requestAnimationFrame(() => {
        deleteChildren(listRef.current);
        listRef.current.append(
          ...data
            .map(user => {
              if (!user.picture || !user.picture[0]) return;
              return (
                <RouterLink
                  href={`/profile/videos/${user.id}`}
                  class={styles.user}
                >
                  <Avatar src={user.picture[0].thumbnailImagePath} />
                  <div class={styles.userInfo}>
                    <span>{user.username}</span>
                    <span class={styles.videoCount}>
                      {user.totalVideos} видео
                    </span>
                  </div>
                </RouterLink>
              );
            })
            .filter(x => !!x)
        );
      })
    );
  fetch(
    "https://api.dolgion.com/dolgion/public/leaderboard/categories/lastmonth"
  )
    .then(res => res.json())
    .then(({ data }) => {
      deleteChildren(categoryListRef.current);
      requestAnimationFrame(() =>
        categoryListRef.current.append(
          ...data.map(category => (
            <RouterLink href={`/category/${category.categoryId}`}>
              <div class={styles.category}>
                <span>#{category.mnName}</span>
                <span class={styles.categoryVideoCount}>
                  {category.totalVideos} видео
                </span>
              </div>
            </RouterLink>
          ))
        )
      );
    });
  return (
    <div class={styles.leaderboard}>
      <p>Энэ сарын шилдэг контент бүтээгчид</p>
      <hr />
      <div class={styles.userList} ref={listRef}>
        <div class={styles.user}>
          <div class={[styles.avatar, loading]} />
          <div class={styles.userInfo}>
            <span class={[loading, styles.loading]} />
            <span class={[styles.videoCount, loading]} style="height: 10px;" />
          </div>
        </div>
      </div>
      <p>Энэ сарын шилдэг ангиллууд</p>
      <hr />
      <div class={styles.categoryList} ref={categoryListRef}>
        <div class={styles.category}>
          <span class={loading} />
          <span class={styles.categoryVideoCount} />
        </div>
      </div>
    </div>
  );
};
