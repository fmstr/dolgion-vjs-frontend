import jsx from "@/jsx";
import VScroll from "@/modules/vscroll";
import useState from "@/modules/reactive";

import styles from "./styles";
import Sidebar from "./Sidebar";
import Category from "./Category";

export default () => {
  let state = useState({ categories: undefined }, { index: 0 });
  fetch("https://api.dolgion.com/dolgion/public/category/popular/list")
    .then(res => res.json())
    .then(
      res =>
        (state.categories = res.data.map(
          ({ description, mnName: name, categoryId: id }) => {
            const category = {
              description,
              name,
              id
            };
            return () => <Category {...category} />;
          }
        ))
    );

  return (
    <div class={styles.page}>
      <div class={styles.sidebar}>
        <Sidebar />
      </div>
      <div
        class={styles.content}
        onMount={node => {
          state.onSet("categories", () => {
            node.append(
              <VScroll
                render={() => {
                  if (state.index >= state.categories.length) return;
                  const node = state.categories[state.index]();
                  state.index++;
                  return node;
                }}
              />
            );
          });
        }}
      />
    </div>
  );
};
