import jsx from "@/jsx";
import { parseTime } from "@/util";
import { RouterLink } from "@/modules/router";

import styles from "./styles";
import Avatar from "@/components/Avatar";
import VideoPlayer from "@/components/VideoPlayer";

export default ({ route: { id } }) => (
  <div
    class={styles.content}
    onMount={node => {
      fetch("https://api.dolgion.com/dolgion/public/video/detail", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ id })
      })
        .then(res => res.json())
        .then(({ data }) => {
          if (!data) return;
          document.title = data.title + " | Dolgion.com";
          requestAnimationFrame(() =>
            node.append(
              <>
                <div class={styles.video}>
                  <VideoPlayer
                    autoplay={true}
                    class={styles.player}
                    id={id}
                    src={
                      data.video && data.video.originalFilePath
                        ? "https://api.dolgion.com/dolgion" +
                          data.video.originalFilePath
                        : null
                    }
                  />
                  <div class={styles.sidebar}>Something here...</div>
                </div>
                <div class={styles.rest}>
                  <span class={styles.title}>{data.title}</span>
                  <div class={styles.userInfo}>
                    <RouterLink
                      href={`/profile/videos/${data.userBasicDto.id}`}
                    >
                      <Avatar
                        src={
                          data.userBasicDto.picture &&
                          data.userBasicDto.picture[0].thumbnailImagePath
                        }
                      />
                    </RouterLink>
                    <div class={styles.userDetail}>
                      <RouterLink
                        href={`/profile/videos/${data.userBasicDto.id}`}
                      >
                        <span style="color: rgba(0,0,0,.7);">
                          {data.userBasicDto.username}
                        </span>
                      </RouterLink>
                      <span>{parseTime(data.createdAt)}</span>
                    </div>
                  </div>
                  <div class={styles.separator} />
                  <div class={styles.videoInfo}>
                    <div class={styles.info}>
                      <img src="/assets/icons/eye.svg" height="15" />
                      <span>{data.viewCount || "0"} үзсэн</span>
                    </div>
                    <div class={styles.info}>
                      <img src="/assets/icons/heart.svg" height="15" />
                      <span>
                        {(data.likeUserIds && data.likeUserIds.length) || "0"}{" "}
                        таалагдсан
                      </span>
                    </div>
                  </div>
                  <div class={styles.description}>{data.description}</div>
                </div>
              </>
            )
          );
        });
    }}
  />
);
