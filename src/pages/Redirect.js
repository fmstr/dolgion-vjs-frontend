import jsx from "@/jsx";

export default ({ route: { to, id } }) => {
  if (!to || !id) document.location.href = "/";
  switch (to) {
    case "l":
      document.location.href = `/live/${id}`;
      return;
    case "pv":
      document.location.href = `/profile/videos/${id}`;
      return;
    default:
      document.location.href = "/";
      return;
  }
};
