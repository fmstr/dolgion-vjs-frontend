import jsx from "@/jsx";
import styles from "./styles";

import LoadingProgress from "@/components/LoadingProgress";

export default () => (
  <div class={styles.loading}>
    <LoadingProgress />
  </div>
);
