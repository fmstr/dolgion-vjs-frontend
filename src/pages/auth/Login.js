import jsx from "@/jsx";
import useState from "@/modules/reactive";
import validation from "@/modules/validation";
import { appState } from "@/state";
import { RouterLink, Redirect } from "@/modules/router";
import { disableEvent, classIf } from "@/util";

import Input from "@/components/Input";
import Button from "@/components/Button";

import styles from "./styles";
import { textDanger, alertDanger } from "@/scss";

export default () => {
  if (appState.token) {
    Redirect("/");
    return <></>;
  }

  const [emailRef, passwordRef] = jsx.createRefs(2);
  let state = useState({
    error: undefined,
    loading: undefined,
    emailError: undefined,
    passwordError: undefined
  });
  const handleError = res =>
    (state.error = (res && res.message) || "Алдаа гарлаа");
  const submit = (_, e) => {
    disableEvent(e);
    const data = {
      email: emailRef.current.value,
      password: passwordRef.current.value
    };
    if (!validation.email.test(data.email)) {
      state.emailError = true;
      return;
    }
    if (!data.password) {
      state.passwordError = true;
      return;
    }

    state.loading = true;
    fetch("https://api.dolgion.com/dolgion/public/user/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    })
      .catch(() => {
        state.loading = false;
        handleError();
      })
      .then(res => {
        state.loading = false;
        return res.json();
      })
      .then(res => {
        if (res.status !== "u12") return handleError(res);
        appState.token = res.data;
        Redirect("/");
      });
  };

  return (
    <div class={styles.page}>
      <div class={styles.wrapper}>
        <h2>Нэвтрэх</h2>
        <form onSubmit={submit}>
          <div class={styles.inputGroup}>
            <div class={styles.alert}>
              <div
                class={alertDanger}
                onMount={node =>
                  state.on("error", error => {
                    node.hidden = !error;
                    node.innerText = error;
                  })
                }
                hidden
              />
            </div>
            <div>
              <Input
                autocomplete="email"
                type="text"
                placeholder="Цахим шуудан"
                ref={emailRef}
                onMount={node => {
                  state.on("emailError", emailError =>
                    classIf(node, emailError, styles.invalid)
                  );
                }}
                onInput={() => {
                  if (!state.emailError && !state.error) return;
                  state.emailError = state.error = false;
                }}
              />
              <small
                class={textDanger}
                onMount={node => {
                  state.on(
                    "emailError",
                    emailError => (node.hidden = !emailError)
                  );
                }}
                hidden
              >
                Имэйл оруулах шаардлагатай
              </small>
            </div>
            <Input
              autocomplete="password"
              type="password"
              placeholder="Нууц үг"
              ref={passwordRef}
              onMount={node => {
                state.on("passwordError", passwordError =>
                  classIf(node, passwordError, styles.invalid)
                );
              }}
              onInput={() => {
                if (!state.passwordError && !state.error) return;
                state.passwordError = state.error = false;
              }}
            />
            <div class={styles.textCenter}>
              <RouterLink href="/forgot">Нууц үг сэргээх</RouterLink>
            </div>
          </div>
          <div class={styles.separator} />
          <div class={styles.socialLogin}>
            <div class={styles.iconWrapper}>
              <img src="/assets/icons/facebook-f.svg" />
            </div>
            <div class={styles.iconWrapper}>
              <img src="/assets/icons/google-plus-g.svg" />
            </div>
          </div>
          <Button type="submit" class={styles.button}>
            <img
              class={styles.spin}
              src="/assets/icons/cog.svg"
              onMount={node =>
                state.on("loading", loading => (node.hidden = !loading))
              }
              hidden
            />
            Нэвтрэх
          </Button>
        </form>
      </div>
    </div>
  );
};
