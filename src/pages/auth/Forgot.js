import jsx from "@/jsx";
import { Redirect } from "@/modules/router";
import { appState } from "@/state";

import Input from "@/components/Input";
import Button from "@/components/Button";
import styles from "./styles";

export default () => {
  if (appState.token) {
    Redirect("/");
    return <></>;
  }

  return (
    <div class={styles.page}>
      <div class={styles.wrapper}>
        <h2>Нууц үг сэргээх</h2>
        <form>
          <div class={styles.inputGroup}>
            <Input
              autocomplete="email"
              type="text"
              placeholder="Цахим шуудан"
            />
          </div>
          <Button type="submit" class={styles.button}>
            Илгээх
          </Button>
        </form>
      </div>
    </div>
  );
};
