import jsx from "@/jsx";
import useState from "@/modules/reactive";
import { Redirect } from "@/modules/router";
import { appState } from "@/state";

import styles from "./styles";
import Input from "@/components/Input";
import Button from "@/components/Button";

export default () => {
  if (appState.token) {
    Redirect("/");
    return <></>;
  }

  let state = useState({ loading: undefined });
  return (
    <div class={styles.page}>
      <div class={styles.wrapper}>
        <h2>Бүртгүүлэх</h2>
        <form>
          <div class={styles.inputGroup}>
            <Input
              autocomplete="username"
              type="text"
              placeholder="Хэрэглэгчийн нэр"
            />
            <Input
              autocomplete="email"
              type="text"
              placeholder="Цахим шуудан"
            />
            <Input
              autocomplete="password"
              type="password"
              placeholder="Нууц үг"
            />
            <Input
              autocomplete="current-password"
              type="password"
              placeholder="Нууц үг давтана уу"
            />
          </div>
          <div class={styles.separator} />
          <div class={styles.socialLogin}>
            <div class={styles.iconWrapper}>
              <img src="/assets/icons/facebook-f.svg" />
            </div>
            <div class={styles.iconWrapper}>
              <img src="/assets/icons/google-plus-g.svg" />
            </div>
          </div>
          <Button type="submit" class={styles.button}>
            <img
              class={styles.spin}
              src="/assets/icons/cog.svg"
              onMount={node =>
                state.on("loading", loading => (node.hidden = !loading))
              }
              hidden
            />
            Бүртгүүлэх
          </Button>
        </form>
      </div>
    </div>
  );
};
