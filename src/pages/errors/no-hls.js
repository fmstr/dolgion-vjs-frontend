import jsx from "@/jsx";
import { RouterLink } from "@/modules/router";

import { errorPage } from "./styles";

export default () => (
  <div class={errorPage}>
    Your browser does not support HLS. Please update it to appropriate version.{" "}
    <RouterLink href="/">Home</RouterLink>
  </div>
);
