import jsx from "@/jsx";
import { Redirect } from "@/modules/router";
import { disableEvent } from "@/util";

import { errorPage } from "./styles";

export default () => (
  <div class={errorPage}>
    Ийм хуудас олдсонгүй.
    <a
      onClick={(_, event) => {
        disableEvent(event);
        if (history.length > 0) history.back();
        else Redirect("/");
      }}
    >
      Буцах
    </a>
  </div>
);
