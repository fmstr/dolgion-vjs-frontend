export function loadImg(src) {
  return new Promise(resolve => {
    const img = new Image();
    img.onload = resolve;
    img.src = src;
  });
}
export function setBgImage(node, src) {
  node.style.backgroundImage = `url("${src}")`;
}
export function disableEvent(event) {
  event.preventDefault();
  event.stopPropagation();
}

export function classIf(node, val, className) {
  if (val) node.classList.add(className);
  else node.classList.remove(className);
}
export function deleteChildren(node) {
  while (node.firstChild) node.firstChild.remove();
}

export function debounceAnimationFrame(fn) {
  let animationFrame;
  return (...args) => {
    if (animationFrame) cancelAnimationFrame(animationFrame);
    animationFrame = requestAnimationFrame(() => fn(...args));
  };
}

export function parseIsoDatetime(dtstr) {
  var dt = dtstr.split(/[: T-]/).map(parseFloat);
  return new Date(
    dt[0],
    dt[1] - 1,
    dt[2],
    dt[3] || 0,
    dt[4] || 0,
    dt[5] || 0,
    0
  );
}

const hour = 60;
const day = hour * 24;
const week = day * 7;

export function parseTime(time) {
  time = parseIsoDatetime(time);
  let currentTime = new Date();
  const timeDiff = (currentTime - time) / 1000 / 60 - 8 * 60; // Server time difference is 8 hours

  if (timeDiff < 1) return "Хормын өмнө";
  if (timeDiff < hour) return `${Math.floor(timeDiff)} минутын өмнө`;
  if (timeDiff < day) return `${Math.floor(timeDiff / 60)} цагийн өмнө`;
  if (timeDiff < week) return `${Math.floor(timeDiff / 60 / 24)} хоногийн өмнө`;

  const mm = time.getMonth() + 1;
  const dd = time.getDate();
  return [
    time.getFullYear(),
    (mm > 9 ? "" : "0") + mm,
    (dd > 9 ? "" : "0") + dd
  ].join("-");
}
