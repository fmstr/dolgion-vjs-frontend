const config = require("@nicepack/core")(options => {
  options.babel.plugins.push([
    "@babel/plugin-transform-react-jsx",
    { pragma: "jsx.h", pragmaFrag: "jsx.hFrag" }
  ]);
});

config.devServer = { host: "0.0.0.0", compress: true, overlay: true };
config.entry = ["./polyfill/index.js", ...config.entry];
module.exports = config;
